# -*- coding: iso-8859-15 -*-

import os
from PIL import Image
import PCV
from PCV.geometry.camera import Camera
from pylab import *
from numpy import *
from scipy import linalg
import fundamental as fm
import matplotlib.pyplot as plt

'''
DESENHAR LINHAS EPIPOLARES

Entrada: correspondência de coord. homogêneas
Saída: Linhas epipolares

Neste exemplo são plotados os pares de coord. numa imagem e as 
linhas epipolares na outra imagem.

A partir de 8 pares de coordenadas homogêneas correspondentes
em duas imagens, obtemos F e consequentemente as linhas epipolares.
Os 8 pares bastam para definir as linhas. Dadas duas câmeras fixas 
(supõe-se este o caso), os 8 pares de coord. representam 8 pontos P 
do R3 que são capturados pelas câmeras. 

Cada par de coord gera uma linha epipolar.

A fazer:
- Plotar epipolos (opção em plot_epipolar_line)
- Plotar linhas epipolares nas duas imagens

'''
def epipolar_draw(az, n=7):
	draw_epipoles(az['F'], az['homog'], az['imgs'][0], az['imgs'][1], n)

def draw_epipoles(F, homog, im2, im1, n=5):
	if n >= homog['x1'].shape[1]: 
		return None 

	# Figura 0
	figure()
	imshow(im1)
	for i in range(n): plot_epipolar_line(im1, F, homog['x2'][:,i],False)	# Linhas epipolares
	# for i in range(n): plot(homog['x1'][0,i], homog['x1'][1,i],'o')			# Coord.
	axis('off')

	# Figura 1
	figure()
	imshow(im2)
	# for i in range(n): plot_epipolar_line(im2, F, homog['x1'][:,i],False)
	for i in range(n): plot(homog['x2'][0,i],homog['x2'][1,i],'o')
	axis('off')
	show()

def compute_epipole(F):
    U,S,V = linalg.svd(F)
    e = V[-1]
    return e/e[2]
    
    
def plot_epipolar_line(im, F, x, show_epipole = True):
    m,n = im.shape[:2]
    line = dot(F,x)
    t = linspace(0,n,100)
    lt = array([(line[2]+line[0]*tt)/(-line[1]) for tt in t])
    ndx = (lt>=0) & (lt<m) 
    plot(t[ndx],lt[ndx],linewidth=2)
    if show_epipole:
    	epipole = compute_epipole(F)
    	plot(epipole[0]/epipole[2],epipole[1]/epipole[2],'r*')


def calc_fundamental_mat(x1, x2):
	return fm.fundamental_mat(x1, x2)

	
def homogenize(correspondence, points2D, x1_idx=2, x2_idx=1):
	idx = (correspondence[:,0] >= 0) & (correspondence[:,1] >= 0)
	x1 = points2D[x1_idx][:,correspondence[idx,0]]
	x1 = vstack( (x1,ones(x1.shape[1])) )
	x2 = points2D[x2_idx][:,correspondence[idx,1]]
	x2 = vstack( (x2,ones(x2.shape[1])) )
	return { 'x1': x1, 'x2': x2 }


def is_image(fi):
	if fi.endswith(".jpg") or fi.endswith(".pgm") or fi.endswith(".png"): return True
	return False

def load_imgs(folder):
	imgs = []
	folder += 'images/'
	for fi in os.listdir(folder):
		if is_image(fi):
			imgs.append(array(Image.open(folder+fi).convert("RGB")))
	return imgs

def load_txt(folder, ext = ".txt"):
	txts = []
	for fi in os.listdir(folder):
		if fi.endswith(ext):
			txts.append(loadtxt(folder+fi))
	return txts

def load_gen_txt(folder, ext = ".txt"):
	txts = []
	for fi in os.listdir(folder):
		if fi.endswith(ext):
			txts.append(genfromtxt(folder+fi, dtype='int', missing='*'))
	return txts


def load_folder(folder):
	points2D = [vec.T for vec in load_txt(folder + '2D/', '.corners')]
	correspondence = genfromtxt(folder+'2D/nview-corners',dtype='int',missing='*')
	homog = homogenize(correspondence, points2D, 0, 1)
	F = calc_fundamental_mat(homog['x1'],homog['x2'])
	return {'F': F, 'homog': homog, 'imgs': load_imgs(folder)}


def load_folder_single(folder):
	points2D = [loadtxt(folder + '2D/000.corners').T, loadtxt(folder + '2D/001.corners').T]
	correspondence = genfromtxt(folder+'2D/nview-corners',dtype='int',missing='*')
	homog = homogenize(correspondence, points2D, 0, 1)
	F = calc_fundamental_mat(homog['x1'],homog['x2'])
	return {'F': F, 'homog': homog, 'imgs': load_imgs(folder)}


def load_ox():
	folder = 'stereo/ox/'
	points2D = [vec.T for vec in load_txt(folder + '2D/', '.corners')]
	correspondence = genfromtxt(folder+'2D/bt.nview-corners',dtype='int',missing='*')
	homog = homogenize(correspondence, points2D, 0, 1)
	F = calc_fundamental_mat(homog['x1'],homog['x2'])
	return {'F': F, 'homog': homog, 'imgs': load_imgs(folder)}

def load_merton():
	folder = 'stereo/merton/'
	points2D = [vec.T for vec in load_txt(folder + '2D/', '.corners')]
	correspondence = genfromtxt(folder+'2D/nview-corners',dtype='int',missing='*')
	homog = homogenize(correspondence, points2D, 2, 1)
	F = calc_fundamental_mat(homog['x1'],homog['x2'])
	return {'F': F, 'homog': homog, 'imgs': load_imgs(folder)}

def plot_2d_points(img_hash):
		# make 3D points homogeneous and project
	X = vstack( (img_hash['points3D'][0],ones(img_hash['points3D'][0].shape[1])) )
	x = img_hash['camera'][0].project(X)

	# plotting the points in view 1
	figure()
	imshow(img_hash['image'][0])
	plot(img_hash['points2D'][0],img_hash['points2D'][1],'*')
	axis('off')
	figure()
	imshow(img_hash['image'][1])
	plot(x[0],x[1],'r.')
	axis('off')
	show()