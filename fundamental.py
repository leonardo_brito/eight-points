from numpy import *
from scipy import linalg

def fundamental_mat(x1, x2): 

    n = x1.shape[1]
    if x2.shape[1] != n:
    	return None

    # Normalizar coordenadas
    x1 = x1 / x1[2]
    avg1 = mean(x1[:2],axis=1)
    norm1 = sqrt(2) / std(x1[:2])
    T1 = array([[norm1,0,-norm1 * avg1[0]],[0,norm1,-norm1 * avg1[1]], [0,0,1]])
    
    x2 = x2 / x2[2]
    avg2 = mean(x2[:2],axis=1)
    norm2 = sqrt(2) / std(x2[:2])
    T2 = array([[norm2,0,-norm2 * avg2[0]],[0,norm2,-norm2 * avg2[1]], [0,0,1]])

    x1 = dot(T1,x1)
    x2 = dot(T2,x2)

    # Calcular F com coordenadas normalizadas
    num_pts = x1.shape[1]
    
    # Matriz A de ordem 3 x num_pts
    # AF = 0
    A = zeros((num_pts, 9))
    for i in range(num_pts):
        A[i] = [x1[0,i]*x2[0,i], x1[0,i]*x2[1,i], x1[0,i]*x2[2,i],
                x1[1,i]*x2[0,i], x1[1,i]*x2[1,i], x1[1,i]*x2[2,i],
                x1[2,i]*x2[0,i], x1[2,i]*x2[1,i], x1[2,i]*x2[2,i] ]
            
    # Calcular solução dos mínimos quadrados
    U,S,V = linalg.svd(A)
    F = V[-1].reshape(3,3)
    U,S,V = linalg.svd(F)
    S[2] = 0
    F = dot(U, dot(diag(S), V))

	# Reverter normalização
    F = dot(T1.T, dot(F,T2))

    return F/F[2,2]

