import Tkinter
from PIL import Image, ImageTk
from sys import argv

window = Tkinter.Tk(className="bla")

image = Image.open(argv[1] if len(argv) >=2 else "bla2.png")
f = open(argv[2], 'w')
canvas = Tkinter.Canvas(window, width=image.size[0], height=image.size[1])
canvas.pack()
image_tk = ImageTk.PhotoImage(image)
canvas.create_image(image.size[0]//2, image.size[1]//2, image=image_tk)

def callback(event):
	print("%d.0 %d.0" % (event.x, event.y))
	f.write("%d.0 %d.0\n" % (event.x, event.y))

def close_window(): 
	window.destroy()
	f.close()

canvas.bind("<Button-1>", callback)
Tkinter.mainloop()